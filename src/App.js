import Header from './components/Header.jsx'
import Screen from './components/Screen.jsx';

function App() {
  return (
    <div className="App">
      <Header />
      <Screen />
    </div>
  );
}

export default App;
