import React from 'react'

function Loader() {
    return (
        <figure id='loader'>
            <img src={process.env.PUBLIC_URL + '/img/pokeball.png'} alt='Loading...' height='100' width='100'></img>
        </figure>
    )
}

export default Loader
